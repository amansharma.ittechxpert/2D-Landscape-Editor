package org.openrsc.editor;

import org.openrsc.editor.Utils.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {

    public static String cacheDir = System.getProperty("user.home");
    public static int logVerbosity = 3;
    private static final String CLIENT_CACHE_PATH = "Client cache path";

    public static void loadSettings() {
        Properties props = new Properties();

        try {
            File settingsFile = new File("Settings.conf");
            if (!settingsFile.exists()) {
                Logger.Error("Could not find settings file at " + settingsFile.getAbsolutePath());
                return;
            } else {
                Logger.Info("Settings file path: " + settingsFile.getAbsolutePath());
            }

            File configFile = new File("Settings.conf");
            if (!configFile.isDirectory()) {
                if (!configFile.exists()) {
                    saveSettings();
                    return;
                }
            }

            FileInputStream in = new FileInputStream("Settings.conf");
            props.load(in);
            in.close();

            cacheDir = getPropString(props, CLIENT_CACHE_PATH, cacheDir);

        } catch (Exception e) {
            Logger.Warn("Warning: Error loading Settings.conf!");
            e.printStackTrace();
        }
    }

    private static String getPropString(Properties props, String key, String defaultProp) {
        String value = props.getProperty(key);
        if (value == null) return defaultProp;
        return value;
    }

    public static void saveSettings() {
        try {
            Properties props = new Properties();

            props.setProperty(CLIENT_CACHE_PATH, cacheDir);

            FileOutputStream out = new FileOutputStream("Settings.conf");
            props.store(out, "---Open RuneScape Classic 2D landscape editor config---");
            out.close();
        } catch (IOException e) {
            Logger.Error("Could not save launcher settings!");
            e.printStackTrace();
        }
    }
}
